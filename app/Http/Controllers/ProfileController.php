<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;     //ini berguna untuk menselect id user yang telah login intinya user yg login saja yg bisa kita liat,update datanya
use App\Profile;

class ProfileController extends Controller
{
    public function index(){
        $profile = Profile::where('user_id', Auth::id())->first();
        return view('profile.index', compact('profile'));
    }

    public function update(Request $request, $id){
        $request->validate([
            'umur' => 'required',
            'bio' => 'required',
            'alamatr' => 'required'
        ]);
        $profile = Profile::find($id);
        
        $profile->umur = $request['umur'];
        $profile->bio = $request['bio'];
        $profile->alamatr = $request['alamatr'];
 
        $profile->save();  
        
        return redirect('/profile');
    }
}
