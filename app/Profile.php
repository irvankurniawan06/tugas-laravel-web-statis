<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profile' ;
    protected $fillable = ['umur','bio','alamatr','user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
