<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="../../index3.html" class="brand-link">
      <img src="{{asset('admin/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Admin</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('admin/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          @guest  <!--menampilkan nam blom login yaitu:anda blom login -->
          <a href="#" class="d-block">Anda Belum Login</a>
          @endguest
          @auth  <!--menampilkan nama yg sudah login sesuai dengan nama yg login misalnya yg login irvan maka namanya juga irvan ok -->
          <a href="#" class="d-block">{{Auth::user()->name}} ({{(Auth::user()->profile->umur)}})</a>
          @endauth
          
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu --> 
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               <li class="nav-item">
               <a href="/" class="nav-link">
                 <i class="nav-icon fas fa-tachometer-alt"></i>
                 <p>
                   Dashboard
                 </p>
               </a>
             </li>
               
          <li class="nav-item">
            <a href="/data-table" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Data Table
              </p>
            </a>
          </li>
          
          @auth  <!--Fungsi ini untuk ketika kita ngk login maka side ini akan di sembunyikan yang bisa mengakses side ini hanya yg udah login -->
              
          <li class="nav-item">
            <a href="/genre" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Genre
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/film" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Film
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/cast" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Cast
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="/profile" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Profile
              </p>
            </a>
          </li>


          <li class="nav-item">
            <a class="nav-link bg-danger" href="{{ route('logout') }}"
            onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
              <i class="nav-icon fas fa-sign-out" aria-hidden="true"></i>
              <p>
              Logout
              </p>
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>

          </li>
          @endauth

          @guest <!-- fungsi ini di gunakan ketika blom login maka pakai ini biar lansung d arahkan ke login ok irvan-->
          <li class="nav-item">
            <a href="/film" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Film
              </p>
            </a>
            <li class="nav-item">
              <a href="/login" class="nav-link bg-primary">
                <i class="nav-icon fas fa-th"></i>
                <p>
                  Login
                </p>
              </a>
            </li>   
          @endguest

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
