@extends('layout.master')

@section('judul')
Detail Gendre Film
@endsection

@section('content')

<h1>Gendre Film {{$genre->nama}}</h1>
<p>Ini Merupakan Sebuah Gendre Film yaitu {{$genre->nama}}</p>
<div class="row">
    @foreach ($genre->film as $item)
        
    <div class="col-4">
        <div class="card" style="width: 18rem;">
            <img  src="{{asset('gambar_poster/'. $item->poster)}}" class="card-img-top"alt="Card image cap">
            <div class="card-body">
              <h3>{{$item->judul}}</h3>
              <p class="card-text">{{$item->ringkasan}}.</p>
            </div>
        </div>
    </div>
    @endforeach
</div>


@endsection