@extends('layout.master')

@section('judul')
Edit Gendre
@endsection

@section('content')

<form action="/genre/{{$genre->id}}" method="post">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama Gendre</label>
      <input type="text" name="nama" value="{{$genre->nama}}" class="form-control" >
    </div>
    @error('genre')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
   
    
    <button type="submit" class="btn btn-primary">Edit</button>
  </form>


@endsection