@extends('layout.master')

@section('judul')
list Gendre Film
@endsection

@section('content')

<a href="/genre/create" class="btn btn-secondary mb-3">Tambah Genre</a>


<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama Gendre</th>
        <th scope="col">Action</th>
        <th scope="col">List Film</th>

      </tr>
    </thead>
    <tbody>
        @forelse ($genre as $key => $item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item -> nama}}</td>
            <td>
              <ul>
                @foreach ($item->film as $value)
                    <li>{{$value->judul}}</li>
                @endforeach

              </ul>
            </td>
            <td>
               
                <form action="/genre/{{$item->id}}" method="post">
                    <a href="/genre/{{$item->id}}" class= "btn btn-info btn-sm">Detail </a>
                    <a href="/genre/{{$item->id}}/edit" class= "btn btn-warning btn-sm">Edit Genre</a>
                    @csrf
                    @method('delete')
                    <input type="submit" class="btn btn-danger btn-sm" value="delete">
                </form>
            </td>
          </tr>
        @empty
            
        @endforelse

    </tbody>
</table>

@endsection