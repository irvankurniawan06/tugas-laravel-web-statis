@extends('layout.master')

@section('judul')
Halaman List Film
@endsection
@section('content')

@auth
<a href="/film/create" class="btn btn-secondary mb-3">Tambah Film</a>
@endauth

<div class="row">
    @forelse ($film as $item)
    <div class="col-4">
        <div class="card" style="width: 18rem;">
            <img  src="{{asset('gambar_poster/'. $item->poster)}}" class="card-img-top"alt="Card image cap">
            <div class="card-body">
              <span class="badge badge-info">{{$item->genre->nama}}</span>
              <h5>{{$item->judul}}</h5>
              <p class="card-text">{{Str::limit($item->ringkasan, 30)}}.</p>

              @auth
              <form action="/film/{{$item->id}}" method="post">
                @csrf
                @method('delete')
                    <a href="/film/{{$item->id}}" class= "btn btn-info btn-sm">Detail </a>
                    <a href="/film/{{$item->id}}/edit" class= "btn btn-warning btn-sm">Edit</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="delete">
                </form>
                  
              @endauth

              @guest
              <a href="/film/{{$item->id}}" class= "btn btn-info btn-sm">Detail </a>
              @endguest
            </div>
          </div>
    </div>
        
    @empty
        <H4>Data Film Belum ada</H4>
    @endforelse
</div>

@endsection