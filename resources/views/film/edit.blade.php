@extends('layout.master')

@section('judul')
Halaman Edit Film
@endsection

@section('content')
<form action="/film/{{$film->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Judul Film</label>
      <input type="text" value="{{$film->judul}}" name="judul" class="form-control" >
    </div>
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
    <label>Ringkasan</label>
      <textarea name="ringkasan" class="form-control">{{$film->ringkasan}}</textarea>
     </div>
    @error('ringkasan')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Tahun</label>
        <input type="number "value="{{$film->tahun}}" name="tahun" class="form-control" >
      </div>
      @error('tahun')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror  
    <div class="form-group">
        <label>Poster</label>
        <input type="file" name="poster" class="form-control" >
      </div>
      @error('poster')
      <div  value="{{$film->poster}}" class="alert alert-danger">{{ $message }}</div>
      @enderror 
    <div class="form-group">
        <label>Kategori</label>
        <select name="genre_id" class="form-control" id="">
            <option value="">---Pilih Kategori---</option>
            @foreach ($genre as $item)
            @if ($item->id === $film->genre_id)

                <option value="{{$item->id}}" selected> {{$item->nama}}</option>
            @else 
                <option value="{{$item->id}}"> {{$item->nama}}</option>
            @endif
                
            @endforeach

        </select>
      </div>
      @error('genre_id')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror


    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection