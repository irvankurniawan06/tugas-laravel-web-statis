@extends('layout.master')

@section('judul')
Film {{$film->judul}}

@endsection



@section('content')
<img src="{{asset('/gambar_poster/'. $film->poster)}}" alt="">
<h1>{{$film->judul}}</h1>
<p> {{$film->ringkasan}}</p>




<h1>Komentar</h1>

@foreach ($film->kritik as $item)
<div class="card">
  <div class="card-body">
    <h3>{{$item->user->name}}</h3>
      <p class="card-text">{{$item->content}}</p>
      <p class="card-text">Rating:{{$item->point}}/10</p>
      
  </div>
</div>
@endforeach

<form action="/komentar" method="POST" enctype="multipart/form-data" class="my-3">
    @csrf
    
    <div class="form-group">
    <label>Content</label>
      <input type="hidden" name="film_id" value="{{$film->id}}" id="">
      <textarea name="content" class="form-control"></textarea>
     </div>
    @error('content')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Rating Film (Batas dari 1-10)</label>
      <input type="hidden" name="film_id" value="{{$film->id}}" id="" >
      <input type="number" name="point" class="form-control" >
    </div>
    @error('point')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror  
   

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

<a href="/film" class="btn btn-secondary">Kembali </a>


@endsection