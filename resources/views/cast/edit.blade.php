@extends('layout.master')

@section('judul')
Edit Cast
@endsection

@section('content')

<form action="/cast/{{$cast->id}}" method="post">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" value="{{$cast->nama}}" class="form-control" >
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Umur</label>
        <input type="number" name="umur" value="{{$cast->umur}}" class="form-control" >
      </div>
      @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror   
    <div class="form-group">
      <label >Bio</label>
      <input name="bio" value="{{$cast->bio}}" class="form-control" >
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <button type="submit" class="btn btn-primary">Edit</button>
  </form>


@endsection