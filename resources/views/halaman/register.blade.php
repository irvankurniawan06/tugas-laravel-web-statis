@extends('layout.master')

@section('judul')
Halaman Form
@endsection

@section('content')
    <h1> Buat Account Baru </h1>
    <h3>Sign Up Form</h3>
     <form action="/welcome" method="post">
     @csrf
        <label>First name</label><br>
        <input type="text" name="firstname"><br><br>
        <label>Last name</label><br>
        <input type="text" name="lastname"><br><br>
        <label>Gender</label><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br><br>
        <label>Usia</label> <br>
        <input type="number" name="usia"> <br><br>
        <label>Nationality</label><br>
        <select name="nationality" id="">
            <option value="Indonesia">Indonesia</option>
            <option value="Inggris">Inggris</option>
            <option value="china">China</option>
            <option value="Rusia">Rusia</option>
        </select> <br> <br>
        <label>Language Spoken</label><br>
        <input type="checkbox" name="lenguage">Bahasa Indonesia<br>
        <input type="checkbox" name="lenguage">English<br>
        <input type="checkbox" name="lenguage">Orther<br><br>
        <label>Bio</label><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br><br>
     <input type="submit" value="Sign Up">
     </form>
@endsection