@extends('layout.master')

@section('judul')
Halaman Index
@endsection

@section('content')
    <a href="/register">Kembali ke menu sebelumnya</a>
    <h1>SELAMAT DATANG {{$namadepan}} {{$namabelakang}}</h1>
    <h3>Terima kasih tela bergabung di website kami. Media Belajar kita semua</h3>
@endsection