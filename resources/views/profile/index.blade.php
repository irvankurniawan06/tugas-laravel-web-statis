@extends('layout.master')

@section('judul')
Halaman Update Profile
@endsection

@push('script')
<script src="https://cdn.tiny.cloud/1/ti2dib4i91222rkqb2hmr9o15or9sc72hjgu58tsee9h3goj/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: 'textarea',
    plugins: 'a11ychecker advcode casechange export formatpainter image editimage linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tableofcontents tinycomments tinymcespellchecker',
    toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter image editimage pageembed permanentpen table tableofcontents',
    toolbar_mode: 'floating',
    tinycomments_mode: 'embedded',
    tinycomments_author: 'Author name',
  });
</script> 
@endpush

@section('content')

<form action="/profile/{{$profile->id}}" method="post">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama User</label>
      <input type="text"  value="{{$profile->user->name}}" class="form-control" disabled >
    </div>
     
    <div class="form-group">
      <label>Email User</label>
      <input type="text"  value="{{$profile->user->email}}" class="form-control" disabled >
    </div>
   
    <div class="form-group">
      <label>Umur Profile</label>
      <input type="number" name="umur" value="{{$profile->umur}}" class="form-control" >
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror  
    <div class="form-group">
      <label >Biodata</label>
      <textarea name="bio" class="form-control" cols="10" rows="5">{{$profile->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Alamat</label>
      <textarea name="alamatr" class="form-control" cols="10" rows="5">{{$profile->alamatr}}</textarea>
    </div>
    @error('alamatr')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <button type="submit" class="btn btn-primary">Edit Profile</button>
  </form>


@endsection