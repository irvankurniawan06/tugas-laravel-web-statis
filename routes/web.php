<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "IndexController@index");

Route::get('/register', "AuthController@bio");

Route::post('/welcome', "AuthController@kirim");

Route::get('/data-table', function(){
    return view('table.data-table');
});

Route::group(['middleware' => ['auth']], function(){
 //CRUD Gendre
    //create
    Route::get('/genre/create', 'GenreController@create');
    Route::post('/genre', 'GenreController@store');
    //Read
    Route::get('/genre', 'GenreController@index');
    Route::get('/genre/{genre_id}','GenreController@show');
    //update
    Route::get('/genre/{genre_id}/edit', 'GenreController@edit');
    Route::put('/genre/{genre_id}', 'GenreController@update');
    //delete
    Route::delete('/genre/{genre_id}', 'GenreController@destroy');
 
 //CRUD Cast
    //Create
    Route::get('/cast/create', 'CastController@create'); //route menuju form create
    Route::post('/cast', 'CastController@store'); //route untuk menyimpan data ke data base

    //Read
    Route::get('/cast', 'CastController@index');// route list cast
    Route::get('/cast/{cast_id}','CastController@show'); //route detail cast

    //update
    Route::get('/cast/{cast_id}/edit', 'CastController@edit'); // route menuju ke edit cast
    Route::put('/cast/{cast_id}', 'CastController@update');// route unruk update berdasarkan cast yang berada d data base , ingat irvan cast itu database yah bukan yang lain, dan inget lagi  di form edit tambahkan @method nya ('PUT') sesudah @scrf ,,biasanya metode ini di gunakan untuk update

    //delete
    Route::delete('/cast/{cast_id}', 'CastController@destroy'); //route untuk menghapus data cast tadi di database

    
    // Profile
    //update Profile
    Route::resource('profile', 'ProfileController')->only([
        'index' , 'update'
    ]);
    //komentar
    Route::resource('komentar', 'KritikController')->only([
        'index' , 'store'
    ]); 
    
});

//CRUD Film
    Route::resource('film', 'FilmController');



    




Auth::routes();

